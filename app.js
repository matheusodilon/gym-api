var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyparser = require('body-parser');
var cors = require('cors');

var indexRouter = require('./routes/create');
var usersRouter = require('./routes/users');
var userRouter = require('./routes/user');
var deleteRouter = require('./routes/delete');
var updateRouter = require('./routes/update');

var instrumentsCreate = require('./routes/instruments/create');
var instrumentsDelete = require('./routes/instruments/delete');
var instrumentsUpdate = require('./routes/instruments/update');
var instrumentsListAll = require('./routes/instruments/instruments');
var instrumentsGetById = require('./routes/instruments/instrument');

var loginRouter = require('./routes/login');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyparser.urlencoded({ extended: true }));

app.use('/create', indexRouter);
app.use('/list-all', usersRouter); // GET USERS
app.use('/list', userRouter); // GET USER
app.use('/delete', deleteRouter); // GET USER
app.use('/update', updateRouter);

app.use('/instruments/create', instrumentsCreate);
app.use('/instruments/list-all', instrumentsListAll);
app.use('/instruments/list', instrumentsGetById);
app.use('/instruments/delete', instrumentsDelete);
app.use('/instruments/update', instrumentsUpdate);
app.use('/login', loginRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
