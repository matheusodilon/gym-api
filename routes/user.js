var express = require('express');
var router = express.Router();
var admin = require("firebase-admin");

let db = admin.firestore();

/* GET users listing. */
router.get('/:id', function(req, res, next) {
 db.collection('users').doc(req.params.id).get().then((item) => {
    const user = item.data();
    user.id = item.id;
    res.send(JSON.stringify(user));
  });

});

module.exports = router;
