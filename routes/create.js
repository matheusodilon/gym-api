var express = require('express');
var router = express.Router();

var admin = require("firebase-admin");

var serviceAccount = require("../service-account-file.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://gymapp-c628b.firebaseio.com"
});
let db = admin.firestore();

/* GET home page. */
router.post('/', function(req, res, next) {
  // res.render('index', { title: 'Express' });
  //Insert key,value pair to database
  console.log(req.body);
  let docRef = db.collection('users');
  
  docRef.add(req.body)
    .then(() => { 
      res.send('criado com sucesso!');
    });
});

module.exports = router;
