var express = require('express');
var router = express.Router();
var admin = require("firebase-admin");

let db = admin.firestore();

router.post('/', function(req, res, next) {
  let dataRecieved = req.body;

  db.collection('users')
    .where("email", "==", dataRecieved.email)
    .where("password", "==", btoa(dataRecieved.password))
    .get()
    .then((item) => {
      // console.log(item);
      if(item.size === 0) {
        res.send(null);
        return;
      }
      const user = item.docs[0].data();
      user.id = item.id;
      res.send(JSON.stringify(user));
    });

});

module.exports = router;
