var express = require('express');
var router = express.Router();
var admin = require("firebase-admin");

let db = admin.firestore();

/* GET users listing. */
router.delete('/:id', function(req, res, next) {
  db.collection('instruments').doc(req.params.id).delete()
    .then((deletedItem) => {
      res.send('Deleted Success!');
    });
});

module.exports = router;
