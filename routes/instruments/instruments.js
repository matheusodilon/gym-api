var express = require('express');
var router = express.Router();
var admin = require("firebase-admin");

let db = admin.firestore();

/* GET users listing. */
router.get('/', function(req, res, next) {
  const docs = [];
  db.collection('instruments').get()
  .then((snapshot) => {
    snapshot.forEach((doc) => {
      const instrument = doc.data();
      instrument.id = doc.id;

      docs.push(instrument);
      console.log(doc.id, '=>', doc.data());
    });
  })
  .catch((err) => {
    console.log('Error getting documents', err);
  }).finally(() => {
    console.log(docs);
    res.send(JSON.stringify(docs));
  });
});

module.exports = router;
