var express = require('express');
var router = express.Router();
var admin = require("firebase-admin");

let db = admin.firestore();

/* GET users listing. */
router.get('/:id', function(req, res, next) {
 db.collection('instrument').doc(req.params.id).get().then((item) => {
    const instrument = item.data();
    instrument.id = item.id;
    res.send(JSON.stringify(instrument));
  });

});

module.exports = router;
