var express = require('express');
var router = express.Router();
var admin = require("firebase-admin");

let db = admin.firestore();

/* GET users listing. */
router.put('/:id', function(req, res, next) {
  db.collection("instruments").doc(req.params.id).update(req.body);
  res.send('Updated Success!');
});

module.exports = router;
